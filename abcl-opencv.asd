(in-package #:asdf-user)

(defsystem :abcl-opencv
  :version "0.0.0"
  :depends-on (:jss)
  :serial t
  :components ((:mvn "org.openpnp/opencv" :version "3.2.0-1")
	       (:file "package")
	       (:file "java-capture")))
