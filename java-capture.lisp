(in-package :abcl-opencv)

(defun test ()
  (let ((i 0))
    (defun try-capture ()
      (format t "attempt: ~A~%" (1+ i))
      (incf i)
      (new 'VideoCapture)))
  (handler-case (try-capture)
    (error ()
      (handler-case (#"loadLibrary" 'System #"Core.NATIVE_LIBRARY_NAME")
	(error ()
	  (#"setProperty" 'System
			  "java.library.path"
			  (format nil "~A:~A~A"
				  (#"getProperty" 'System "java.library.path")
				  (user-homedir-pathname)
				  ".m2/repository/org/openpnp/opencv/3.2.0-1"))
	  (try-capture))))))





